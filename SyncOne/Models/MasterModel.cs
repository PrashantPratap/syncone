﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SyncOne.Models
{
    public class MasterModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}