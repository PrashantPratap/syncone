﻿using SyncOne.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SyncOne.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public JsonResult MasterData(int id)
        {
            List<MasterModel> x = new List<Models.MasterModel>()
            {
                new Models.MasterModel() { ID = "1", Name = "One", Value=$"One_1_{id}" },
                new Models.MasterModel() { ID = "2", Name = "Two", Value=$"Two_2_{id}" },
                new Models.MasterModel() { ID = "3", Name = "Two", Value=$"Two_3_{id}" }
            };
            return new JsonResult() { Data =  new { result = x }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult ChildData(string id)
        {
            List<MasterModel> x = new List<Models.MasterModel>()
            {
                new Models.MasterModel() { ID = "11", Name = "One_One", Value=$"One_One_1_{id}" },
                new Models.MasterModel() { ID = "12", Name = "One_Two", Value=$"One_Two_2_{id}" },
                new Models.MasterModel() { ID = "13", Name = "One_Two", Value=$"One_Two_3_{id}" }
            };
            return new JsonResult() { Data = new { result = x }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}